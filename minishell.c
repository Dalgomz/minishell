#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

/*
BUGS : 
Redir no sirve
BG funciona a medias, no lo ejecuta en segundo plano
*/


// Para manejo facil con bool
typedef int bool;
#define true 1
#define false 0

// Constantes para la separacion de comando
#define END -1
#define UNREAD 0
#define PIPE 1
#define AMPER 2
#define PYC 3

// Constantes para pipes
#define UNDEF -1
#define PIPEREAD  0
#define PIPEWRITE 1

// Constantes para máximos
#define MAXBUFF 257
#define MAXARGS 50
#define MAXESPC	3

// Constantes para palabras reservadas
#define QUIT -1
#define BG 1
#define REDIR 2
#define ECHO 3
#define VAR 4

void buscar(char** completo, int aux[]);
int makeargv(char *s, char *delimiters, char ***argvp);
int ejecutar (char** argv,int next_input, int sigtype);

int main(int argc, char *argv[]){

	// Declaración de variables
	/* 
	linea - 	Donde se lee del terminal
	full - 		Linea que contiene todos los comandos
	argsv - 	Guarda un subcomando para ejecutarlo, inicia con todo en NULL
	vars -		Guarda los datos de las variables que se pueden usar con ECHO

	quit  - 	Controla a salida del shell, inicia en false
	background- Controla el uso de BG
	redir -		Controla el uso de REDIR

	sigtype - 	Que tipo de conexión hay entre dos sub-comandos
	valor - 	Valor de retorno despues de las ejecuciones
	reservas - 	Array de 3 espacios que contiene las posibles palabras especiales
	i - 		Usada en ciclos FOR
	nvars -		El numero de variables almacenadas actualmente
	next_input-	Guarda el descriptor de salida en caso de pipe
	argsc - 	Número de strings que hay en "full"
	ini - 		Se usa para marcar el inicio de un subcomando
	fin - 		Se usa para marcar el final de un subcomando
	index - 	Posicion actual que se está leyendo de "full"
	n_comandos-	El numero de comandos con pipe ejecutados
	inputfd -	Descrpitor de archivo con la nueva entrada
	outputfd -	Descrpitor de archivo con la nueva salida

	pid - 		id de proceso usado en BG
	redirpid - 	Id del proceso que hace REDIR
	*/
	char linea [MAXBUFF];
	char **full, *argsv[MAXARGS], *vars[2][MAXARGS];
	bool quit, background, redir;
	int sigtype, valor, reservas[MAXESPC], i, nvars, next_input;
	int argsc, ini, fin, index, n_comandos, inputfd, outputfd;
	pid_t pid, redirpid;

	quit = false;
	nvars = 0;

	for(i = 0; i<MAXARGS;i++){
		argsv[i] = NULL;
	}

	printf("=== === === === === === ===\n");
	printf("=== MiniSHELL v1.0.1.17 ===\n");
	printf("=== === === === === === ===\n");

	// Ciclo que mantiene corriendo el shell
	while(!quit){
		printf("MiniShell> ");

		// Control de la linea de comandos
		if (!fgets(linea, MAXBUFF, stdin)) {
			printf("Error de entrada\n");
			return 0;
		}

		// Separa las palabras de linea y guarda el número de palabras que tiene
		argsc = makeargv(linea, " \n", &full);

		// Inicio seguro para nueva ejecución de una línea
		n_comandos = 0;
		index = 0;
		ini = 0;
		fin = 0;
		inputfd = UNDEF;
		outputfd = UNDEF;
		next_input = UNDEF;
		background = false;
		redir = false;

		// Buscar palabras especiales, si las hay mueve el inicio
		buscar(full, reservas);

		// Si no se ejecuta el comando "QUIT", "ECHO", o se asigna una variable
		if(reservas[0] == QUIT){
			quit = true;
		}
		else if(reservas[0] == VAR){

			// Busca la variable, por si ya existe
			for(i = 0;i<nvars;i++){
				if(strcmp(vars[0][i],full[0]) == 0){
					vars[1][i] = full[2];
					i = -1;
					break;
				}
			}
			if(i != -1){
				// En la matriz de variables, guarda en la fila 0 el nombre
				vars[0][nvars] = malloc(strlen(full[0])+1*sizeof(char));
				strcpy(vars[0][nvars],full[0]);

				// En la matriz de variables, guarda en la fila 1 el valor
				vars[1][nvars] = malloc(strlen(full[2])+1*sizeof(char));
				strcpy(vars[1][nvars],full[2]);
			
				nvars++;
			}
		}
		else if(reservas[0] == ECHO){
			// Busca si la variable existe y la imprime
			for(i = 0;i<nvars;i++){
				if(strcmp(vars[0][i],full[1]) == 0){
					printf("%s -> %s\n", vars[0][i], vars[1][i]);
					i = -1;
					break;
				}
			}
			if(i!=-1){
				printf("Variable %s no encontrada\n", full[1]);
			}
		}
		// Ejecucion de un comando normal
		else{
			// El proceso se va a correr en segundo plano
			if(reservas[0] == BG || reservas[1] == BG){
				ini++;
				index++;
				fin++;
				background = true;
				pid = fork();
			}

			// El proceso tiene redireccion de entrada/salida
			if(reservas[0] == REDIR || reservas[1] == REDIR){
				ini++;
				index++;
				fin++;
				if(background == true){
					// Si ya está en segundo plano y es el hijo
					if(pid == 0){
						if(strcmp(full[argsc-3],"INPUTFILE") == 0){
							inputfd= open(full[argsc-1],O_RDONLY);
							dup2(inputfd,STDIN_FILENO);
							close(inputfd);

							if(argsc > 6)
							if(strcmp(full[argsc-6],"OUTPUTFILE") == 0){
								outputfd = open(full[argsc-4],O_WRONLY|O_CREAT,0644);
								dup2(outputfd,STDOUT_FILENO);
								close(outputfd);
							}
						}
						else if(strcmp(full[argsc-3],"OUTPUTFILE") == 0){
							outputfd = open(full[argsc-1],O_WRONLY|O_CREAT,0644);
							dup2(outputfd,STDOUT_FILENO);
							close(outputfd);

							if(argsc > 6)
							if(strcmp(full[argsc-6],"INPUTFILE") == 0){
								inputfd= open(full[argsc-4],O_RDONLY);
								dup2(inputfd,STDIN_FILENO);
								close(inputfd);
							}
						}
					}
				}
				// Si no se debe ejecutar en segundo plano
				if(background == false){	
					redir = true;
					redirpid = fork();
					if(redirpid == 0){
						
						if(strcmp(full[argsc-3],"INPUTFILE") == 0){
							inputfd= open(full[argsc-1],O_RDONLY);
							dup2(inputfd,STDIN_FILENO);
							close(inputfd);

							if(argsc > 6)
							if(strcmp(full[argsc-6],"OUTPUTFILE") == 0){
								outputfd = open(full[argsc-4],O_WRONLY|O_CREAT,0644);
								dup2(outputfd,STDOUT_FILENO);
								close(outputfd);
							}
						}
						else if(strcmp(full[argsc-3],"OUTPUTFILE") == 0){
							outputfd = open(full[argsc-1],O_WRONLY|O_CREAT,0644);
							dup2(outputfd,STDOUT_FILENO);
							close(outputfd);

							if(argsc > 6)
							if(strcmp(full[argsc-6],"INPUTFILE") == 0){
								inputfd = open(full[argsc-4],O_RDONLY);
								dup2(inputfd,STDIN_FILENO);
								close(inputfd);
							}
						}
					}			
				}
			}

			if(background == true){
				if(pid == 0){

					while(index<argsc){ 
						// Si despues de un "&&" hay errores de ejecución
						if(sigtype == AMPER){
							for(i = 0; i< n_comandos-1;i++)
								wait(NULL);
							wait(&valor);
							n_comandos = 0;
							if(WIFEXITED(valor)){
								valor = WEXITSTATUS(valor);
							}
							if(valor == -1){
								printf("Error de ejecución &&\n");
								break;
							}
						}

						if(sigtype == PYC){
							for(i = 0; i< n_comandos;i++)
								wait(NULL);
							n_comandos = 0;
							wait(NULL);
						}
						// Buscar un separador de comandos
						while(index<argsc){
							// Si hay más comandos por recorrer
							if(full[index] == NULL)	
								break;
							// Si no es ningun conector de comando
							else if(strcmp(full[index],"|") != 0 && strcmp(full[index],"&&") != 0 && strcmp(full[index],";") != 0
								&&  strcmp(full[index],"INPUTFILE") != 0 && strcmp(full[index],"OUTPUTFILE") != 0){ 
								index++;
								fin++;
							}
							// Si encuentra un separador
							else{
								// Guarda el tipo de separador de comando en sigtype
								if(strcmp(full[index],"|") == 0)
									sigtype = PIPE;
								else if(strcmp(full[index],"&&") == 0)
									sigtype = AMPER;
								else if(strcmp(full[index],";") == 0)
									sigtype = PYC;
								index++;
								break;
							}
						}

						// Si la linea de comandos no tiene más subcomandos por leer
						if (index == argsc)
							sigtype = END;
						else if(strcmp(full[index-1],"INPUTFILE") == 0 ||strcmp(full[index-1],"OUTPUTFILE") == 0){
							sigtype = END;
						}

						// Llena argsv con un subcomando
						for(i = 0;i<fin-ini;i++){
							argsv[i] = malloc(MAXBUFF*sizeof(char));

							strcpy(argsv[i],full[ini + i]);
						}
						argsv[i] = NULL;

						if(sigtype == PYC){
							next_input = ejecutar(argsv, next_input, sigtype);
						}
						else if(sigtype == AMPER){
							next_input = ejecutar(argsv, next_input, sigtype);
						}
						else if(sigtype == PIPE){
							n_comandos++;
							next_input = ejecutar(argsv, next_input, sigtype);
						}
						else{
							next_input = ejecutar(argsv, next_input, sigtype);	
						}

						// Se prepara para ejecutar el siguiente subcomando
						for(i = 0;i<fin-ini;i++){
							free(argsv[i]);
							argsv[i] = NULL;
						}
						fin++;
						ini = fin;
					}

					if(sigtype == END){
						for(i = -1; i< n_comandos;i++)
							wait(NULL);
						n_comandos = 0;
					}
					while(true);
				}
			} // if background = true

			else if(redir == true){
				if(redirpid == 0){

					while(index<argsc){ 
						
						// Si despues de un "&&" hay errores de ejecución
						if(sigtype == AMPER){
							// Esperar por los forks de los pipes
							for(i = 0; i< n_comandos;i++)
								wait(NULL);
							n_comandos = 0;
							wait(&valor);
							if(WIFEXITED(valor)){
								valor = WEXITSTATUS(valor);
							}
							if(valor != 0){
								printf("Error de ejecución &&\n");
								break;
							}
						}

						if(sigtype == PYC){
							// Esperar por los forks de los pipes
							for(i = 0; i< n_comandos;i++)
								wait(NULL);
							n_comandos = 0;
							wait(NULL);
						}
						// Buscar un separador de comandos
						while(index<argsc){

							// Si hay más comandos por recorrer
							if(full[index] == NULL)	
								break;
							// Si no es ningun conector de comando
							else if(strcmp(full[index],"|") != 0 && strcmp(full[index],"&&") != 0 && strcmp(full[index],";") != 0
								&&  strcmp(full[index],"INPUTFILE") != 0 && strcmp(full[index],"OUTPUTFILE") != 0){

								index++;
								fin++;
							}
							// Si encuentra un separador
							else{
								// Guarda el tipo de separador de comando en sigtype
								if(strcmp(full[index],"|") == 0)
									sigtype = PIPE;
								else if(strcmp(full[index],"&&") == 0)
									sigtype = AMPER;
								else if(strcmp(full[index],";") == 0)
									sigtype = PYC;
								index++;
								break;
							}
						}

						// Si la linea de comandos no tiene más subcomandos por leer
						if (index == argsc)
							sigtype = END;
						else if(strcmp(full[index-1],"INPUTFILE") == 0 ||strcmp(full[index-1],"OUTPUTFILE") == 0){
							sigtype = END;
						}

						// Llena argsv con un subcomando
						for(i = 0;i<fin-ini;i++){
							argsv[i] = malloc(MAXBUFF*sizeof(char));
							strcpy(argsv[i],full[ini + i]);
						}
						argsv[i] = NULL;

						if(sigtype == PYC){
							next_input = ejecutar(argsv, next_input, sigtype);
						}
						else if(sigtype == AMPER){
							next_input = ejecutar(argsv, next_input, sigtype);
						}
						else if(sigtype == PIPE){
							n_comandos++;
							next_input = ejecutar(argsv, next_input, sigtype);
						}
						else{
							next_input = ejecutar(argsv, next_input, sigtype);	
						}

						// Se prepara para ejecutar el siguiente subcomando
						for(i = 0;i<fin-ini;i++){
							free(argsv[i]);
							argsv[i] = NULL;
						}
						fin++;
						ini = fin;
						
					}
					exit(0);
				} // Hijo del redir
			}
			else{ // if no redir 
				while(index<argsc){ 
					
					// Si despues de un "&&" hay errores de ejecución
					if(sigtype == AMPER){

						// Esperar por los forks de los pipes
						for(i = 0; i< n_comandos;i++)
							wait(NULL);
						n_comandos = 0;
						wait(&valor);
						if(WIFEXITED(valor)){
							valor = WEXITSTATUS(valor);
						}
						if(valor != 0){
							printf("Error de ejecución &&\n");
							break;
						}
					}

					if(sigtype == PYC){
						// Esperar por los forks de los pipes
						for(i = 0; i< n_comandos;i++)
							wait(NULL);
						n_comandos = 0;
						wait(NULL);
					}
					// Buscar un separador de comandos
					while(index<argsc){

						// Si hay más comandos por recorrer
						if(full[index] == NULL)	
							break;
						// Si no es ningun conector de comando
						else if(strcmp(full[index],"|") != 0 && strcmp(full[index],"&&") != 0 && strcmp(full[index],";") != 0
							&&  strcmp(full[index],"INPUTFILE") != 0 && strcmp(full[index],"OUTPUTFILE") != 0){

							index++;
							fin++;
						}
						// Si encuentra un separador
						else{
							// Guarda el tipo de separador de comando en sigtype
							if(strcmp(full[index],"|") == 0)
								sigtype = PIPE;
							else if(strcmp(full[index],"&&") == 0)
								sigtype = AMPER;
							else if(strcmp(full[index],";") == 0)
								sigtype = PYC;
							
							index++;
							break;
						}
					}

					// Si la linea de comandos no tiene más subcomandos por leer
					if (index == argsc)
						sigtype = END;
					else if(strcmp(full[index-1],"INPUTFILE") == 0 ||strcmp(full[index-1],"OUTPUTFILE") == 0){
						sigtype = END;
					}

					// Llena argsv con un subcomando
					for(i = 0;i<fin-ini;i++){
						argsv[i] = malloc(MAXBUFF*sizeof(char));
						strcpy(argsv[i],full[ini + i]);
					}
					argsv[i] = NULL;

					if(sigtype == PYC){
						next_input = ejecutar(argsv, next_input, sigtype);
					}
					else if(sigtype == AMPER){
						next_input = ejecutar(argsv, next_input, sigtype);
					}
					else if(sigtype == PIPE){
						n_comandos++;
						next_input = ejecutar(argsv, next_input, sigtype);
					}
					else{
						next_input = ejecutar(argsv, next_input, sigtype);	
					}

					// Se prepara para ejecutar el siguiente subcomando
					for(i = 0;i<fin-ini;i++){
						free(argsv[i]);
						argsv[i] = NULL;
					}
					fin++;
					ini = fin;
				}
			} // No background no Redir
			
		}

		if(redir == true){
			if(redirpid == 0){
				for(i = -1; i< n_comandos;i++)
					wait(NULL);
				n_comandos = 0;
				exit(0);
			}
			else
				waitpid(redirpid, NULL, 0);
		}

		if(sigtype == END && background == false){
			for(i = -1; i< n_comandos;i++)
				wait(NULL);
			n_comandos = 0;
		}
	}
	printf("=== === === === === === ===\n");
	printf("===  MiniSHELL Closing  ===\n");
	printf("=== === === === === === ===\n");

	return 0;
}

// BACKGROUND e INPUT OUUTPUT
int ejecutar (char* argv[],int next_input, int sigtype){
	int pipe_fd[2];
 	pid_t pid;

	// Crear pipe
	pipe( pipe_fd );	
	// Crear un hijo (Mantiene intactos los descriptores del padre)
	pid = fork();
 
 
	if (pid == 0) {
		if (sigtype == PIPE && next_input == UNDEF) {
			// Comando con pipe siguiente y el anterior no era pipe
			dup2( pipe_fd[PIPEWRITE], STDOUT_FILENO);
		} 
		else if (sigtype == PIPE && next_input != UNDEF) {
			// Comando con pipe siguiente y el anterior era pipe
			dup2(next_input, STDIN_FILENO);
			dup2(pipe_fd[PIPEWRITE], STDOUT_FILENO);
		} 
		else if(sigtype != PIPE && next_input != UNDEF){
			// Comando sin pipe siguiente y el anterior era pipe
			dup2(next_input, STDIN_FILENO );
			close(pipe_fd[PIPEREAD]);
			pipe_fd[PIPEREAD] = UNDEF;
		} 
		else if(sigtype == END && next_input == UNDEF){
			// Si es el ultimo comando y el anterior no era pipe, cierra el pipe de entrada
			close(pipe_fd[PIPEREAD]);
			pipe_fd[PIPEREAD] = UNDEF;
		}

		if (execvp( argv[0], argv) == -1)
			_exit(EXIT_FAILURE); // If child fails
	}
 
	if (next_input != UNDEF) 
		close(next_input);
 
	// Se cierra el pipe de entrada
	close(pipe_fd[PIPEWRITE]);

	// Devuelve el valor del pipe de lectura, para los siguientes comandos (si fue cerrado es UNDEF)
	return pipe_fd[PIPEREAD];
}

// Función tomada de https://cs.wmich.edu/~trenary/blog/wp-content/uploads/2015/10/makeargv.c
int makeargv(char *s, char *delimiters, char ***argvp)
{
	char *t;
	char *snew;
	int numtokens;
	int i;
	/* snew is real start of string after skipping leading delimiters */
	snew = s + strspn(s, delimiters);
	/* create space for a copy of snew in t */
	if ((t = calloc(strlen(snew) + 1, sizeof(char))) == NULL) {
		*argvp = NULL;
		numtokens = -1;
	} else {                     /* count the number of tokens in snew */
		strcpy(t, snew);
		if (strtok(t, delimiters) == NULL)
			numtokens = 0;
		else
			for (numtokens = 1; strtok(NULL, delimiters) != NULL;
					numtokens++)
				;  
		/* create an argument array to contain ptrs to tokens */
		if ((*argvp = calloc(numtokens + 1, sizeof(char *))) == NULL) {
			free(t);
			numtokens = -1;
		} else {            /* insert pointers to tokens into the array */
			if (numtokens > 0) {
				strcpy(t, snew);
				**argvp = strtok(t, delimiters);
				for (i = 1; i < numtokens + 1; i++)
					*((*argvp) + i) = strtok(NULL, delimiters);
			} else {
				**argvp = NULL;
				free(t);
			}
		}
	}   

	return numtokens;
}

// Revisa los inicios del comando para buscar palabras especiales
void buscar(char** completo,int aux[]){
	int i, j;
	
	for(i=0;i<2;i++){
		aux[i] = 0;
	}

	if(strcmp(completo[0],"QUIT")==0){
		aux[0] = QUIT;
		return;
	}

	if(completo[2] != NULL)
		for(i = 0;i<2;i++){
			if(strcmp(completo[i],"BG")==0){
				aux[i] = BG;
			}
			else if(strcmp(completo[i],"REDIR")==0){
				aux[i] = REDIR;
			}
		}
	else
		for(i = 0;i<1;i++){
			if(strcmp(completo[i],"BG")==0){
				aux[i] = BG;
			}
			else if(strcmp(completo[i],"REDIR")==0){
				aux[i] = REDIR;
			}
		}

	if(strcmp(completo[0],"ECHO")==0){
		aux[0] = ECHO;
	}
	else if(completo[2] != NULL && completo[3] == NULL)
		if(strcmp(completo[1],"=")==0){
			aux[0] = VAR;
		}
	
}